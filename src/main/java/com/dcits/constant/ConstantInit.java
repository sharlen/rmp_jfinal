package com.dcits.constant;

public class ConstantInit {
	
	/**
	 * jdbc Url
	 */
	public static final String db_connection_jdbc_url = "db.jdbcUrl";
	
	/**
	 * jdbc用户名
	 */
	public static final String db_connection_username = "db.user";
	
	/**
	 * jdbc密码
	 */
	public static final String db_connection_password = "db.password";
	
	public static final String db_connection_driver_class = "db.driverClass";
	
	/**
	 * 数据库连接池参数：初始化连接大小
	 */
	public static final String db_initialSize = "db.initialSize";

	/**
	 * 数据库连接池参数：最少连接数
	 */
	public static final String db_minIdle = "db.minIdle";

	/**
	 * 数据库连接池参数：最多连接数
	 */
	public static final String db_maxActive = "db.maxActive";
	
	/**
	 * redis缓存 名称
	 */
	public static final String redis_name = "redis.name";
	
	/**
	 * redis缓存 host
	 */
	public static final String redis_ip = "redis.ip";
	
	/**
	 * redis缓存 端口
	 */
	public static final String redis_port = "redis.port";
	
	/**
	 * redis缓存 密码
	 */
	public static final String redis_password = "redis.password";
	
	/**
	 * redis缓存 超时时间
	 */
	public static final String redis_timeout = "redis.timeout";
	
	/**
	 * 扫描的包
	 */
	public static final String config_scan_package = "config.scan.package";

	/**
	 * 扫描的jar
	 */
	public static final String config_scan_jar = "config.scan.jar";
	
	/**
	 * 开发模式
	 */
	public static final String config_devMode = "config.devMode";
	
	/**
	 * #文件上传大小限制 10 * 1024 * 1024 = 10M
	 */
	public static final String config_maxPostSize_key = "config.maxPostSize";

	/**
	 * # cookie 值的时间
	 */
	public static final String config_maxAge_key = "config.maxAge";

	/**
	 * # 不使用自动登陆时，最大超时时间，单位：分钟
	 */
	public static final String config_session_key = "config.session";
	
	/**
	 *  缓存类型配置
	 */
	public static final String config_cache_type = "config.cache.type";
	
	
	
}
