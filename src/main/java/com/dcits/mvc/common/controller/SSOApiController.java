package com.dcits.mvc.common.controller;

import com.dcits.constant.ConstantReturnCode;
import com.dcits.mvc.base.BaseController;
import com.dcits.mvc.common.model.UserConfig;
import com.dcits.mvc.common.service.UserConfigService;
import com.jfinal.aop.Clear;
import org.apache.commons.lang3.StringUtils;

/**
 * 第三方登陆
 * @author xuwangcheng
 * @version 1.0.0
 * @description
 * @date 2019/9/29 9:50
 */
public class SSOApiController extends BaseController {

    private static final String TOKEN = "859309a70754700ddbaad0321491fb47";
    private static UserConfigService service = new UserConfigService();

    @Clear
    public void getLoginToken () {
        String token = getPara("token");
        if (StringUtils.isBlank(token) || !TOKEN.equalsIgnoreCase(token)) {
            renderError(ConstantReturnCode.TOKEN_ERROR, "token不正确");
            return;
        }

        String username = getPara("username");

        if (StringUtils.isBlank(username)) {
            renderError(ConstantReturnCode.VALIDATE_FAIL, "参数不正确");
            return;
        }

        //判断当前用户是否存在
        UserConfig userConfig = service.findByKey(username);
        if (userConfig == null) {
            service.save(username, "11111111");
            userConfig = service.findByKey(username);
        }

        //放进全局对象
        String loginToken = com.dcits.tool.StringUtils.MD5Encode(username + System.currentTimeMillis());
        getRequest().getServletContext().setAttribute(loginToken, userConfig);

        renderSuccess(loginToken, "创建成功");
    }


}
