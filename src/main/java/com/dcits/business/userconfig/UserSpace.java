package com.dcits.business.userconfig;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.dcits.business.server.ServerType;
import com.dcits.business.server.ViewServerInfo;
import com.dcits.constant.ConstantGlobalAttributeName;
import com.dcits.mvc.common.model.ServerInfo;
import com.dcits.mvc.common.model.UserConfig;
import com.jfinal.core.JFinal;

public class UserSpace {
	
	private UserConfig userConfig;
	private Map<String, List<ViewServerInfo>> servers = new HashMap<String, List<ViewServerInfo>>();
	private Integer serverCount;
	
	public UserSpace() {
		super();
		for (String typeName:ServerType.typeClasses.keySet()) {
			servers.put(typeName, new ArrayList<ViewServerInfo>());
		}		
	}
	public UserSpace(UserConfig userConfig) {
		this();
		this.userConfig = userConfig;
	}
	
	/***************************公共方法**********************************************/
	/**
	 * 查询指定用户空间有没有初始化
	 * @param userKey
	 * @return
	 */
	public static UserSpace getUserSpace(String userKey) {
		Map<String, UserSpace> spaces = getSpaces();
		return spaces.get(userKey);		
	}
	
	
	/**
	 * 获取指定用户空间
	 * @param config
	 * @return
	 */
	public static UserSpace getUserSpace(UserConfig config) {
		Map<String, UserSpace> spaces = getSpaces();
		UserSpace space = spaces.get(config.getUserKey());
		return space;
	}
	
	public static void addUserSpace(UserConfig config) {
		getSpaces().put(config.getUserKey(), new UserSpace(config));	
	}
	
	@SuppressWarnings("unchecked")
	public static Map<String, UserSpace> getSpaces() {
		return (Map<String, UserSpace>) JFinal.me().getServletContext()
				.getAttribute(ConstantGlobalAttributeName.USER_SPACE_ATTRITURE_NAME);
	}
	
	/*****************************公共方法********************************************/
	
	/**
	 * 获取本用户空间下的指定类型指定viewId的正在监控的服务器信息
	 * @param typeName
	 * @param viewId
	 * @return
	 */
	public ViewServerInfo getServerInfo(String typeName, Integer viewId) {
		List<ViewServerInfo> ss = this.servers.get(typeName);
		if (ss != null) {
			for (ViewServerInfo info:ss) {
				if (viewId.equals(info.getViewId())) {
					return info;
				}
			}
		}
		return null;
	}
	
	/**
	 * 根据指定的typeName和viewId删除正在监控列表的服务器信息
	 * @param typeName
	 * @param viewId
	 */
	public void delServerInfo(String typeName, Integer viewId) {
		ViewServerInfo info = getServerInfo(typeName, viewId);
		if (info != null) this.servers.get(typeName).remove(info);
		
	}
	
	public List<ServerInfo> getServerInfoById (Integer id) {
		List<ServerInfo> infos = new ArrayList<ServerInfo>();
		for (List<ViewServerInfo> list:this.servers.values()) {
			for (ViewServerInfo info:list) {
				if (id.equals(info.getId())) {
					infos.add(info);
				}
			}
		}
		return infos;
	}
	
	/**
	 * 实时更新基本信息
	 * @param viewId
	 * @param serverInfo
	 */
	public void updateBaseInfo (Integer id, ServerInfo serverInfo) {
		if (id == null){			
			return;
		}
		List<ServerInfo> infos = getServerInfoById(id);
		for (ServerInfo info:infos) {
			info.setHost(serverInfo.getHost());
			info.setRealHost(serverInfo.getRealHost());
			info.setPort(serverInfo.getPort());
			info.setUsername(serverInfo.getUsername());
			info.setPassword(serverInfo.getPassword());
			info.setTags(serverInfo.getTags());
			info.setParameters(serverInfo.getParameters());	
		}
	}
	
	public UserConfig getUserConfig() {
		return userConfig;
	}
	public void setUserConfig(UserConfig userConfig) {
		this.userConfig = userConfig;
	}
	
	public Map<String, List<ViewServerInfo>> getServers() {
		return servers;
	}
	public void setServers(Map<String, List<ViewServerInfo>> servers) {
		this.servers = servers;
	}
	public Integer getServerCount() {
		this.serverCount = 0;
		for (String typeName:servers.keySet()) {
			serverCount += (servers.get(typeName)).size();
		}
		return serverCount;
	}
	public void setServerCount(Integer serverCount) {
		this.serverCount = serverCount;
	}
	@Override
	public String toString() {
		return "UserSpace [userConfig=" + userConfig + ", servers=" + servers + ", serverCount=" + serverCount + "]";
	}
	
	
	
	
	
}
