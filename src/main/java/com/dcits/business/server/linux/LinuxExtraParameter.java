package com.dcits.business.server.linux;

public class LinuxExtraParameter {
	
	protected String javaHome;
	protected String networkCardName;

	public LinuxExtraParameter(String javaHome, String networkCardName) {
		super();
		this.javaHome = javaHome;
		this.networkCardName = networkCardName;
	}

	public LinuxExtraParameter() {
		super();

	}

    public void setNetworkCardName(String networkCardName) {
        this.networkCardName = networkCardName;
    }

    public String getNetworkCardName() {
        return networkCardName;
    }

    public String getJavaHome() {
		return javaHome;
	}

	public void setJavaHome(String javaHome) {
		this.javaHome = javaHome;
	}
	
	
	
}
